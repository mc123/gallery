<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\GalleryController;
use App\Http\Controllers\DownloadController;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\FacebookOauthController;
use Illuminate\Support\Facades\Http;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/gallery', [GalleryController::class, 'index']);
Route::post('/gallery', [GalleryController::class, 'store']);

Route::get('/gallery/{path}', [GalleryController::class, 'show']);
Route::post('/gallery/{path}', [GalleryController::class, 'storeImage'])->middleware('facebook');
Route::delete('/gallery/{path}', [GalleryController::class, 'destroy'])->where('path','(.*)');

Route::get('/images/{w}x{h}/{path}', [ImageController::class, 'show'])->whereNumber('w')->whereNumber('h')->where('path','(.*)');

Route::get('/download/gallery/{path}', [DownloadController::class, 'download']);

Route::get('/auth/facebook', [FacebookOauthController::class, 'authenticate']);
Route::get('/token', function (Request $request) {});
