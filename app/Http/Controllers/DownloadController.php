<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use ZipArchive;
use App\Http\Controllers\GalleryController;

class DownloadController extends Controller
{
    function download($path)
    {
        $galleryPath = GalleryController::getGalleryPath($path);
        if (File::exists($galleryPath))
        {
            $zip = new ZipArchive;
            $fileName = $path . "-". time() . ".zip";

             if ($zip->open(public_path($fileName), ZipArchive::CREATE) === TRUE)
             {
                 $files = File::files($galleryPath);

                 foreach ($files as $key => $value)
                 {
                     $name = basename($value);
                     $zip->addFile($value, $name);
                 }

                 $zip->close();
             }
             return response()->download(public_path($fileName))->deleteFileAfterSend(true);

        } else {
            return response("Gallery not found", 404);
        }

    }
}
