<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic as Image;

class ImageController extends Controller
{
    function show($w, $h, $path)
    {
        $imgPath = storage_path() . '/app/gallery/' . $path;
        if (File::exists($imgPath))
        {
            $img = $this->resizeImg($imgPath,$w,$h);
            if ($img == null){
                return response()->json(['error'=>"The photo preview can't be generated."], 500);
            }
            return response($img->encode('jpg'), 200)->header('Content-Type', 'image/jpg');
        } else {
            return response()->json(['error'=>'Photo not found'], 404);
        }

    }

    function resizeImg($imgPath,$w,$h)
    {
        list($origWidth, $origHeight) = getimagesize($imgPath);

        //Check if required dimensions are not greater than original
        if ($origWidth < $w || $origHeight <$h)
        {
            return null;
        }

        //Check if at least one of the dimensions is greater than 0
        if ($w == 0 && $h == 0)
        {
            return null;
        }

        //Calculate new dimensions according to ratio
        if ($w  == 0)
        {
             $newWidth = $origWidth*($h/$origHeight);
        } else {
            $newWidth = $w;
        }
        if ($h == 0)
        {
            $newHeight = $origHeight*($w/$origWidth);
        } else {
            $newHeight = $h;
        }
            
        $img = Image::make($imgPath);              
        $img->resize($newWidth, $newHeight);
        return $img;
    }
}
