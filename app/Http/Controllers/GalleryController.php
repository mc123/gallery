<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;


class GalleryController extends Controller
{
    private $rules = array('name' => 'required|not_regex:/\//i');

    function index(Request $request)
    {
        $galleriesPaths = Storage::directories('gallery');
        $galleriesNames = array_map( function($g) {return basename($g);} ,$galleriesPaths);
        $galleriesDetails = [];
        foreach($galleriesNames as $galleryName){
            array_push($galleriesDetails, $this->getGalleryDetails($galleryName));
        }
        return response()->json([
            'galleries' => $galleriesDetails
        ], 200);
    }

    function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules);

        if($validator->fails())
        {
            return response()->json($validator->errors(),400);
        }
        $galleryName = $request->input('name');
        $galleryPath = $this->getGalleryPath($galleryName);
        if (File::exists($galleryPath))
        {
            return response()->json(['error'=>'Gallery with this name already exists'], 409);
        } else {
            Storage::makeDirectory('/gallery/'. $galleryName);
            $galleryDetails = $this->getGalleryDetails($galleryName);
            return response()->json($galleryDetails, 201);
        };
    }
    
    function show($path)
    {
        $galleryPath = $this->getGalleryPath($path);
        if ($galleryPath) {
            $imgs =File::allFiles($galleryPath);
            $imgsInfo = [];
            foreach($imgs as $img){
                $newImage = $this->getImgDetails(basename($img),$path);
                array_push($imgsInfo,$newImage);
            }
            $galleryInfo = $this->getGalleryDetails($path);
            return response()->json(['gallery' => $galleryInfo, 'images' => $imgsInfo],200);
        } else {
            return response()->json(['error'=>'Gallery does not exists'], 404);
        }
    }

    function destroy($path)
    {
        $filePath = storage_path() . '/app/gallery/' . $path;

        if (File::exists($filePath)) {
            File::delete($filePath);
            Storage::deleteDirectory('/gallery/' . $path);
            return response()->json(['error'=>'Gallery/Photo was deleted'], 200);
        } else {
            return response()->json(['error'=>'Gallery/Photo does not exist'], 404);
        }
    }

    function storeImage(Request $request, $path)
    {
        if (empty($request->header('content-type')) || empty($request->file('image')))
        {
            return response()->json(['error'=>'Invalid request - file not found.'], 400);
        }
        $userId = $request->userId;

        $galleryPath = $this->getGalleryPath($path);
        if ($galleryPath)
        {
            $imgName = $userId .  '_' . $request->file('image')->getClientOriginalName();
            $request->image->move($galleryPath, $imgName);
            $img = $this->getImgDetails($imgName,$path);
            return response()->json(['uploaded' => [$img]]);
        } else {
            return response()->json(['error'=>'Gallery not found'], 404);
        }
    }

    private function getGalleryDetails($galleryName)
    {
        return array(
            'path' =>  str_replace(' ', '%20', $galleryName),
            'name' => $galleryName
        );
    }

    public static function getGalleryPath($gallery)
    {
        $galleryPath = storage_path() . '/app/gallery/' . $gallery;

        if (File::exists($galleryPath))
        {
            return $galleryPath;
        } else {
            return null;
        }
    }

    private function getImgDetails($name, $gallery)
    {
        $imgPath = storage_path() . '/app/gallery/'.$gallery.'/'.$name;
        if (!File::exists($imgPath))
        {
            return null;
        } else {
            return array(
                'path' => str_replace(' ', '%20',$name),
                'fullpath' => str_replace(' ', '%20', $gallery . '/' . $name),
                'name' => ucfirst(pathinfo($name,PATHINFO_FILENAME)),
                'modified' => date('c',filemtime($imgPath))
            );
        }
    }
    
}
