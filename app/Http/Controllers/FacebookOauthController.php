<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class FacebookOauthController extends Controller
{
    function authenticate()
    {
        $url = 'https://www.facebook.com/v12.0/dialog/oauth?client_id=' . env('FACEBOOK_CLIENT_ID') .'&redirect_uri=' .env('FACEBOOK_REDIRECT_URI') . '&response_type=token';
        header('Location: '. $url); 
        exit(0);
    }
    
    public static function getFacebookUserId($token) 
    {
        $response = Http::get('https://graph.facebook.com/me?fields=id', [
            'access_token' => $token,
        ]);
        
        $user = $response->json();

        if(isset($user['error']))
        {
            return null;
        } else {
            return $user['id'];
        }
    }
}
