<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Http\Controllers\FacebookOauthController;

class CheckFacebookUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $userId = FacebookOauthController::getFacebookUserId($request->BearerToken());
        if ($userId)
        {
            $request->userId = $userId;
            return $next($request);
        } else
        {
            return response("Error while authorizing facebook user", 401);
        }

        
    }
    
}
