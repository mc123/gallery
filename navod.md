## Návod na spustenie
 Zadanie je naprogramované v jazyku PHP s využitím frameworku Laravel v8. Predpokladá sa nainštalovaný PHP verzia min. 7 a composer. 
 Spustenie pomocou "composer install" a následne spustenie servera pomocou príkazu "php artisan serve --port=80" (predpokladá sa nainštalovaný PHP verzia min. 7 );

## O vypracovaní
Spracovaný a funkčný je každý zo 6 API requestov spomenutých v http://api.programator.sk/docs#. Testovanie prebiehalo cez Postmana.

Cesty k súborom s vypracovaním:
- "app/Http/Controllers" - Gallery, Image, FacebookOauth, Download controllers
- "app/Http/MiddleWare" - middleware na kontrolou FB usera 
- "routes/web.php" - implementácia routovania (všetko vo web, namiesto /api);

### Bonusová úloha
Zadanie je vypracované vrátane bonusovej úlohy. Laravel obsahuje knižnicu na riešenie Oauth = Socialite, avšak  bolo potrebné použiť client_secret, takže nakoniec je autentifikácie vyriešená od základov manuálne.
- request na prihlásenie na FB je cez "/auth/facebook"
- redirect nastavený na "/token", kde je možné vo frontende z parametrov URL získať Access Token, a následne odoslať v hlavičke ako Bearer token
- User Id sa pridáva na začiatok názvu fotografie a je ukončený znakom "_", napr. 123456_obrazok.jpg
- pri akejkoľvek chybe pri získavaní User Id sa vráti HTTP status kód 401

### Doplňujúca úloha - Stiahnutie celej galérie
Volanie na "/download/gallery/{path}", kde "path" reprezentuje názov galérie, vráti všetky fotografie v galérii zabalené vo formáte zip. Ak galéria neexistuje, vráti sa status code 404.

## Návrhy na zlepšenie/upresnenie
Pri vypracovaní riešenia som narazil na niekoľko detailov, ktoré by bolo možné doladiť.

### "/gallery"
- upresniť informácie ohľadom titulnej fotky pre galériu
- upresniť povolenú dĺžku a sadu znakov pre názov galérie (napr. alfanumerické znaky vrátane medzery a max. veľkosť 30)
    - príklad možného konfliktu: používateľ vytvorí názov galérie ako "abc%20abc", čo je totožné s "abc abc" (v súčasnosti sa ukladjú názvy galérií ako adresáre so znakom medzery a následne sa v odpovedi na request v parametri path zamení medzera na "%20" pre ďalšie URL požiadavky)

### "/gallery/{path}"
- upresniť maximálnu veľkosť/rozmery obrázka
- upresniť povolený formát odosielaných obrázkov (v súčasnosti iba .jpg/.jpeg)
- upresniť, čo sa má stať ak galéria už obsahuje obrázok s rovnakým menom (uložiť/nahradiť/vrátiť 409) (v súčasnosti riešené ako nahradenie)

## "/images/{w}x{h}/{path}"
- upresniť čo sa má stať, ak je jeden z parametrov w alebo h väčší ako originálny parameter w a h (v súčasnosti riešené ako status code 500)
- upresniť, čo sa má stať, ak sú oba parameter w a h nula (0) (v súčasnosti riešené ako  status code 500)